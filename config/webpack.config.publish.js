const path = require('path');
const { HashedModuleIdsPlugin } = require('webpack');
const TerserPlugin = require('terser-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');

const dir = {
  STATIC: path.resolve(__dirname, '../static'),
  PUBLISH: path.resolve(__dirname, '../publish'),
  ROOT: path.resolve(__dirname, '..')
};

module.exports = {
  mode: 'production',
  entry: `${dir.ROOT}/src/components/application-form/application-form.js`,
  // Utilize long-term caching by adding content hashes (not compilation hashes) to compiled assets
  output: {
    path: dir.PUBLISH,
    filename: 'index.js',
    libraryTarget: 'commonjs2' // THIS IS THE MOST IMPORTANT LINE! :mindblow: I wasted more than 2 days until realize this was the line most important in all this guide.
  },
  externals: {
    'react': 'commonjs react' // this line is just to use the React dependency of our parent-testing-project instead of using our own React.
  },
  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin({
        terserOptions: {
          warnings: false,
          compress: {
            comparisons: false
          },
          parse: {},
          mangle: true,
          output: {
            comments: false,
            ascii_only: true
          }
        },
        parallel: true,
        cache: true,
        sourceMap: true
      })
    ],
    nodeEnv: 'production',
    sideEffects: true,
    concatenateModules: true
  },
  plugins: [
    new CompressionPlugin({
      algorithm: 'gzip',
      test: /\.js$|\.css$|\.html$/,
      threshold: 10240,
      minRatio: 0.8
    }),
    new HashedModuleIdsPlugin({
      hashFunction: 'sha256',
      hashDigest: 'hex',
      hashDigestLength: 20
    })
  ],
  performance: {
    assetFilter: assetFilename =>
      !/(\.map$)|(^(main\.|favicon\.))/.test(assetFilename),
  },
  resolve: {
    extensions: ['.js']
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /(node_modules|bower_components)/,
      use: { loader: 'babel-loader' }
    },
    {
      test: /\.css$/,
      exclude: path.resolve(__dirname, '../src/components/application-form/application-form.css'),
      use: ['style-loader', 'css-loader?modules=true']
    },
    {
      test: /\.css$/,
      include: path.resolve(__dirname, '../src/components/application-form/application-form.css'),
      use: ['style-loader', 'css-loader']
    },
    {
      test: /\.(jpg|png|gif)$/,
      use: [
        {
          loader: 'url-loader',
          options: {
            // Inline files smaller than 10 kB
            limit: 10 * 1024
          }
        },
        {
          loader: 'image-webpack-loader',
          options: {
            mozjpeg: {
              enabled: false
              // NOTE: mozjpeg is disabled as it causes errors in some Linux environments
              // Try enabling it in your environment by switching the config to:
              // enabled: true,
              // progressive: true,
            },
            gifsicle: {
              interlaced: false
            },
            optipng: {
              optimizationLevel: 7
            },
            pngquant: {
              quality: '65-90',
              speed: 4
            }
          }
        }
      ]
    },
    {
      test: /\.html$/,
      use: 'html-loader'
    }]
  }
};

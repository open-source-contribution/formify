import React, { Component } from 'react';
import { IntlProvider, addLocaleData } from 'react-intl'

import th from 'react-intl/locale-data/th'
import en from 'react-intl/locale-data/en'
import _ from 'lodash'

import classes from './form.css';

import ApplicationForm from '../../components/application-form/application-form'

// import ApplicationForm from 'formify-tool' // use this for production

import translations from '../../translations'
import formConfig from '../../assets/fixtures/fields'
import validator from '../../assets/fixtures/validator'
import Textbox from '../../components/textbox/textbox'

addLocaleData([...en, ...th])

class Form extends Component {
  constructor (props) {
    super(props)
    this.state = {
      formData: {
        'LANGUAGES': "english"
      },
      customFormError: {},
      locale: 'en'
    }
    this.validateField = this.validateField.bind(this)
    this.evaluateCustomField = this.evaluateCustomField.bind(this)
    this.updateFormData = this.updateFormData.bind(this)
  }

  next (automatedFormHasError, formData) {
    const customFormHasError = this.evaluateCustomField()
    const overallFormData = formData;
    
    if (!automatedFormHasError && !customFormHasError) {
      console.log(overallFormData)
      // do the task of form submission
      // Also assign overallFormData to formData and utilize formData in next step
    }
  }

  evaluateCustomField () {
    const errorObj = {
      ...this.state.customFormError
    }
    let hasError = false
    _.forEach(formConfig.step1, (value, key) => {
      if (value.isCustom) {
        const errorMessageList = this.validateField(value.validations)

        _.map(errorMessageList, (error) => {
          if (error.errorMessage !== this.state.customFormError[error.fieldName]) {
            errorObj[error.fieldName] = error.errorMessage
          }
          if (error.errorMessage) {
            hasError = true
          }
        })
      }
    })
    this.setState({
      customFormError: errorObj
    })
    return hasError
  }

  onFieldValueChange (fieldName, type, validator, value) {
    const formData = {
      ...this.state.formData
    }

    formData[fieldName] = value
    this.setState({
      formData: formData
    })
    setTimeout(() => {
      const dependentFields = _.uniq(validator.map((item) => {return item.field}))
      let shouldValidate = false;
      dependentFields.map((item) => {
        if (!shouldValidate) {
          if (this.state.customFormError[item]) {
            shouldValidate = true
          }  
        }
        return item
      })
      if (shouldValidate) {
        this.setError.bind(this)(validator)
      }
    }, 100)
  }

  setError (validator) {
    const errorMessageList = this.validateField(validator)

    const errorObj = {
      ...this.state.customFormError
    }

    _.map(errorMessageList, (error) => {
      if (error.errorMessage !== this.state.customFormError[error.fieldName]) {
        errorObj[error.fieldName] = error.errorMessage
      }
    })

    this.setState({
      customFormError: errorObj
    })
  }

  validateField (fieldValidator) {
    let error = []
    const dependentFields = _.uniq(fieldValidator.map((item) => {return item.field}))
    _.map(dependentFields, (item) => {
      error.push({
        errorMessage: '',
        fieldName: item
      })
    })

    _.map(fieldValidator, (item) => {
      const selectedErrorObjIndex = _.findIndex(error, (obj) => {
        return obj.fieldName === item.field
      })

      const hasError = (error[selectedErrorObjIndex].errorMessage) !== ''

      if (!hasError) {
        const result = validator[item.validationType](this.state.formData[item.field], this.state.formData)
        if (!result) {
          error[selectedErrorObjIndex].errorMessage = item.errorMessage
        }
      }

    })
    return error
  }

  updateFormData (formData) {
    this.setState({
      formData: {
        ...formData
      }
    })
  }

  render() {
    return (<div>
      <header className={classes.header} /> 
      <IntlProvider locale={this.state.locale} messages={translations[this.state.locale]} key={this.state.locale}>
        <div className={classes.formParent}>
          <ApplicationForm formConfig={formConfig.step1} validator={validator} formData={this.state.formData} updateFormData={this.updateFormData} next={this.next.bind(this)}>
            <div number="3">
              <Textbox
                error={this.state.customFormError[formConfig.step1['2'].name]}
                type={formConfig.step1['2'].type}
                value={this.state.formData[formConfig.step1['2'].name]}
                onFieldValueChange={this.onFieldValueChange.bind(this, formConfig.step1['2'].name, 'text', formConfig.step1['2'].validations)}
                onFieldBlur={this.setError.bind(this, formConfig.step1['2'].validations)} 
                label={formConfig.step1['2'].label}
                name={formConfig.step1['2'].name}
                key={`${formConfig.step1['2'].name}_2_text`} />
            </div>
            <div number="6">
              <Textbox
                error={this.state.customFormError[formConfig.step1['5'].name]}
                type={formConfig.step1['5'].type}
                value={this.state.formData[formConfig.step1['5'].name]}
                onFieldValueChange={this.onFieldValueChange.bind(this, formConfig.step1['5'].name, 'text', formConfig.step1['5'].validations)}
                onFieldBlur={this.setError.bind(this, formConfig.step1['5'].validations)} 
                label={formConfig.step1['5'].label}
                name={formConfig.step1['5'].name}
                key={`${formConfig.step1['5'].name}_2_text`} />
            </div>
          </ApplicationForm>
        </div>
      </IntlProvider>
    </div>);
  }
}

export default Form;

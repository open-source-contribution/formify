import React from 'react';
import Form from './form';
import {shallowWithIntl, mountWithIntl} from '../../../helpers/intl-test'

describe('Form container consuming react-formify', () => {
  it('renders without crashing', () => {
    const wrapper = shallowWithIntl(<Form />);
    expect(wrapper).toBeDefined();
  });

  it('click on ApplicationForm next should successfully call Form next and evaluate custom field', () => {
    const spy = jest.spyOn(Form.prototype, 'next')
    const wrapper = mountWithIntl(<Form />);
    expect(wrapper.instance().state.customFormError).toEqual({})

    wrapper.find('button#application-form-next').simulate('click');
    expect(spy).toHaveBeenCalledWith(true, wrapper.instance().state.formData)

    expect(wrapper.instance().state.customFormError).toEqual({
      "MOBILE_NUMBER": "form.error.required",
      "ADDRESS": "form.error.required"
    })
  })
  
  describe('onFieldValueChange function', () => {
    it('should update form state and error state', async() => {
      const wrapper = mountWithIntl(<Form />);


      wrapper.instance().setState({
        customFormError: {
          "MOBILE_NUMBER": "form.error.required"
        }
      })


      expect(wrapper.instance().state.customFormError).toEqual({
        "MOBILE_NUMBER": "form.error.required"
      });

      const fieldValidator = [{
        "validationType": "required",
        "errorMessage": "form.error.required",
        "field": "MOBILE_NUMBER"
      }]
 
      const formData = wrapper.instance().state.formData
      wrapper.instance().onFieldValueChange('MOBILE_NUMBER', 'number', fieldValidator, '9899')
      expect(wrapper.instance().state.formData).toEqual({
        ...formData,
        "MOBILE_NUMBER": '9899'
      });


      // waiting for few millisecond for form state update. Updated form state is utilized by validate field
      const timeout = ms => new Promise(res => setTimeout(res, ms))
      await timeout(500)
      expect(wrapper.instance().state.customFormError).toEqual({
        "MOBILE_NUMBER": ""
      });
    });  
  })
})

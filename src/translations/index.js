// https://github.com/yahoo/react-intl/wiki/Upgrade-Guide#change-how-messages-are-formatted
// React Intl v2 no longer supports nested messages objects,
// instead the collection of translated string messages passed to <IntlProvider> must be flat.
// This is an explicit design choice which simplifies while increasing flexibility.
// React Intl v2 does not apply any special semantics to strings with dots; e.g., "namespaced.string_id".

import en from './en'
import th from './th'

export default {
  en,
  th
}

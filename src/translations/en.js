const en = {
  'form.NATIONALITY.label': 'Nationality',
  'form.NATIONALITY.option1': 'Indian',
  'form.NATIONALITY.option2': 'American',
  'form.FIRST_NAME.label': 'First name',
  'form.BIRTH_DATE.label': 'Birth date',
  'form.TITLE_CODE.label': 'Title Code',
  'form.TITLE_CODE.option1': 'MR.',
  'form.TITLE_CODE.option2': 'MRS.',
  'form.TITLE_CODE.option3': 'MISS.',
  'form.LANGUAGES.label': 'Languages',
  'form.LANGUAGES.option1': 'English',
  'form.LANGUAGES.option2': 'Hindi',
  'form.LANGUAGES.option3': 'Spanish',
  'form.MOBILE_NUMBER.label': 'Mobile number',
  'form.ADDRESS.label': 'Address',
  'form.ADDRESS_STAY_DURATION.label': 'Address Stay duration',
  'form.MARTITAL_STATUS.label': 'Marital Status',
  'form.MARTITAL_STATUS.option1': 'Single',
  'form.MARTITAL_STATUS.option2': 'Married',
  'form.SPOUSE_NAME.label': 'Spouse name',
  // 'form.SPOUSE_BIRTH_DATE.label': 'Spouse birth date',
  'common.next': 'NEXT',
  //errors
  'form.error.required': 'Required',
  'form.error.invalidDate': 'Enter valid date',
  'form.error.yearLimit': 'Enter valid year',
  'form.error.ageLimit': 'Age should be greater than 20 and less than 65',
  'form.error.stayLessThanAge': 'Stay duration should be less than age'
}

module.exports = en
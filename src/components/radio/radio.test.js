import React from 'react';
import Radio from './radio'
import {mountWithIntl} from '../../../helpers/intl-test'


describe('Radio', () => {
  it('renders without crashing', () => {
    const wrapper1 = mountWithIntl(<Radio
      error={''}
      type={'radioSingleLine'}
      options={[{
        "name": "form.TITLE_CODE.option1",
        "value": "MR."
      },
      {
        "name": "form.TITLE_CODE.option2",
        "value": "MRS."
      },
      {
        "name": "form.TITLE_CODE.option3",
        "value": "MISS."
      }]}
      value={''}
      onFieldValueChange={() => {}}
      label={'form.TITLE_CODE.label'}
      name={'TITLE_CODE'}
    />
    );
    expect(wrapper1).toBeDefined();

    const wrapper2 = mountWithIntl(<Radio
      error={''}
      type={'radioMultiLine'}
      options={[{
        "name": "form.TITLE_CODE.option1",
        "value": "MR."
      },
      {
        "name": "form.TITLE_CODE.option2",
        "value": "MRS."
      },
      {
        "name": "form.TITLE_CODE.option3",
        "value": "MISS."
      }]}
      value={''}
      onFieldValueChange={() => {}}
      label={'form.TITLE_CODE.label'}
      name={'TITLE_CODE'}
    />
    );
    expect(wrapper2).toBeDefined();

  });
})
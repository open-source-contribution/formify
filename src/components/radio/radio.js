import React, { Component } from 'react'
import { Row, Col, ControlLabel, FormGroup, HelpBlock } from 'react-bootstrap'
import PropTypes from 'prop-types'
import { FormattedMessage, injectIntl } from 'react-intl'
import classes from './radio.css'

class RadioButtonSlabs extends Component {
  
  render() {
    const { label, error, options, name, value, type, onFieldValueChange } = this.props
    let width
    if (type === 'radioSingleLine') {
      width = 12 / options.length
    } else {
      width = 12
    }
    let optionsRender = <div />
    if (options && options.length > 0) {
      optionsRender = options.map((item, index) => {
        let className = ''
        if (width < 12) {
          if (index === 0) {
            className = ` ${classes.topLeftBottomLeftBorder}`
          } else if (index === options.length - 1) {
            className = ` ${classes.topRightBottomRightBorder}`
          }
        } else if (index === 0) {
          className = ` ${classes.topLeftTopRightBorder}`
        } else if (index === options.length - 1) {
          className = ` ${classes.bottomLeftBottomRightBorder}`
        }
        return (
          <Col xs={width} key={`${name}-option-${index}-col`} className={(item.value === value ? classes.activeRadio : classes.inActiveRadio) + className}>
            <input 
              id={`${name}-${index}`} 
              type={type === 'radioSingleLine' || type === 'radioMultiLine' ? 'radio' : type}  
              value={item.value}
              onChange={(event) => {
                onFieldValueChange(event.target.value)
              }}
              name={name} 
              className={classes.customRadioButton} />
            <label htmlFor={`${name}-${index}`} key={`${name}-option-${index}-label`}>
            <FormattedMessage id={item.name} key={`${name}-formatMessage`} /></label>
          </Col>
        )
      })
    }

    return (
      <FormGroup validationState={error ? 'error' : null}>
        <Row key={`${name}-formGroup`}>
          <Col
            key={`${name}-col11`}
            componentClass={ControlLabel} xs={12}
          >
            <FormattedMessage id={label} key={`${name}-formatMessage`} />
          </Col>
          <Col
            key={`${name}-col12`} xs={12}
          >
            <Row key={`${name}-row`}>
              {optionsRender}
            </Row>
          </Col>
          {error && <Col xs={12}>
            <HelpBlock key={`${name}-helpBlock`}>
              <FormattedMessage id={error} key={`${name}-formatMessage`} />
            </HelpBlock>
            </Col>}
        </Row>
      </FormGroup>
    )
  }
}

RadioButtonSlabs.propTypes = {
  label: PropTypes.string.isRequired,
  error: PropTypes.string,
  options: PropTypes.array.isRequired,
  name: PropTypes.string.isRequired,
  value: PropTypes.string,
  type: PropTypes.string.isRequired,
  onFieldValueChange: PropTypes.func.isRequired,
}

export default injectIntl(RadioButtonSlabs)

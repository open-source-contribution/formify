
import React from 'react';
import ApplicationForm from './application-form';
import fieldConfig from '../../assets/fixtures/test-data/fields.json'
import validator from '../../assets/fixtures/test-data/validator'
import {shallowWithIntl, mountWithIntl} from '../../../helpers/intl-test'

describe('Application-form', () => {
  it('renders without crashing', () => {
    const wrapper = shallowWithIntl(<ApplicationForm 
      formConfig={fieldConfig.step1} 
      validator={validator} 
      updateFormData={() => {}} 
      next={() =>{}} />
    );
    expect(wrapper).toBeDefined();
  });
  
  it('click on next should set error if field validations are not success', () => {
    const wrapper = mountWithIntl(<ApplicationForm 
      formConfig={fieldConfig.step1} 
      validator={validator} 
      updateFormData={() => {}} 
      next={() =>{}} />
    );
    expect(wrapper.instance().state.error).toEqual({});
  
    wrapper.find('button#application-form-next').simulate('click');
    expect(wrapper.instance().state.error).toEqual({
      "BIRTH_DATE": "form.error.invalidDate",
      "FIRST_NAME": "form.error.required",
      "MARTITAL_STATUS": "form.error.required",
      "NATIONALITY": "form.error.required",
      "TITLE_CODE": "form.error.required",
      "ADDRESS_STAY_DURATION": "form.error.required"
    });
  });
  
  it('click on next should call parent next function with current data and error status', () => {
    const next = jest.fn()
    const wrapper = mountWithIntl(<ApplicationForm
      formData={{
        "BIRTH_DATE": "02/02/2018",
        "FIRST_NAME": "ABC"
      }} 
      formConfig={fieldConfig.step1} 
      validator={validator} 
      updateFormData={() => {}} 
      next={next} />
    );
  
    wrapper.find('button#application-form-next').simulate('click');
    expect(next).toHaveBeenCalledWith(true, {
      "BIRTH_DATE": "02/02/2018",
      "FIRST_NAME": "ABC"
    });
  });
  
  describe('onFieldValueChange function', () => {
    it('should update form state, error state and call updateFormData for data syncing', async() => {
      const next = jest.fn()
      const updateFormData = jest.fn()
      const wrapper = mountWithIntl(<ApplicationForm
        formData={{
          "BIRTH_DATE": "02/02/2018",
          "FIRST_NAME": ""
        }} 
        formConfig={fieldConfig.step1} 
        validator={validator} 
        updateFormData={updateFormData}
        next={next} />
      );


      wrapper.instance().setState({
        error: {
          "FIRST_NAME": "form.error.required"
        }
      })


      expect(wrapper.instance().state.error).toEqual({
        "FIRST_NAME": "form.error.required"
      });

      const fieldValidator = [{
        "validationType": "required",
        "errorMessage": "form.error.required",
        "field": "FIRST_NAME"
      }]
 
      wrapper.instance().onFieldValueChange('FIRST_NAME', 'text', fieldValidator, 'NEW')
      expect(wrapper.instance().state.form).toEqual({
        "BIRTH_DATE": "02/02/2018",
        "FIRST_NAME": "NEW"
      });

      expect(updateFormData).toHaveBeenCalledWith({
        "BIRTH_DATE": "02/02/2018",
        "FIRST_NAME": "NEW"
      })

      // waiting for few millisecond for form state update. Updated form state is utilized by validate field
      const timeout = ms => new Promise(res => setTimeout(res, ms))
      await timeout(500)
      expect(wrapper.instance().state.error).toEqual({
        "FIRST_NAME": ""
      });

    });  
  })

  describe('componentWillReceiveProps function', () => {
    it('componentWillReceiveProps will update formState whenever there is a change in formData', () => {
      const next = jest.fn()
      const updateFormData = jest.fn()
      const wrapper = mountWithIntl(<ApplicationForm
        formData={{
          "BIRTH_DATE": "02/02/2018",
          "FIRST_NAME": "",
          "CUSTOM_FIELD": "ABC"
        }} 
        formConfig={fieldConfig.step1} 
        validator={validator} 
        updateFormData={updateFormData}
        next={next} />
      );

      wrapper.setProps({
        formData: {
          "BIRTH_DATE": "02/02/2018",
          "FIRST_NAME": "",
          "CUSTOM_FIELD": "NEW"
        }
      })
      
      expect(wrapper.instance().state.form).toEqual({
        "BIRTH_DATE": "02/02/2018",
        "FIRST_NAME": "",
        "CUSTOM_FIELD": "NEW"
      });

    });  
  })

  describe('custom field', () => {
    it('custom field should be render correctly for single and multiple child', () => {

      const newFormConfig1 = {
        step1: [
          ...fieldConfig.step1,
          {
            "isCustom": true
          }
        ]
      }

      const newFormConfig2 = {
        step1: [
          ...newFormConfig1.step1,
          {
            "isCustom": true
          }
        ]
      }

      const next = jest.fn()
      const updateFormData = jest.fn()
      const wrapper1 = mountWithIntl(<ApplicationForm
        formData={{
          "BIRTH_DATE": "02/02/2018",
          "FIRST_NAME": "",
          "CUSTOM_FIELD": "ABC"
        }} 
        formConfig={newFormConfig1.step1} 
        validator={validator} 
        updateFormData={updateFormData}
        next={next}>
          <div number="8">
          </div>
        </ApplicationForm>
      );

      const wrapper2 = mountWithIntl(<ApplicationForm
        formData={{
          "BIRTH_DATE": "02/02/2018",
          "FIRST_NAME": "",
          "CUSTOM_FIELD": "ABC"
        }} 
        formConfig={newFormConfig2.step1} 
        validator={validator} 
        updateFormData={updateFormData}
        next={next}>
          <div number="8">
          </div>
          <div number="9">
          </div>
        </ApplicationForm>
      );


      expect(wrapper1).toBeDefined();
      expect(wrapper2).toBeDefined();

    });  
  })


})

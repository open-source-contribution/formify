import React, { Component } from 'react'
import _ from 'lodash'
import PropTypes from 'prop-types'
import { FormattedMessage } from 'react-intl'
import moment from 'moment'
import Dropdown from '../dropdown/dropdown'
import Textbox from '../textbox/textbox'
import DateSelect from '../date-select/date-select'
import {Button, Row, Col} from 'react-bootstrap'
import Radio from '../radio/radio'
import './application-form.css'

class ApplicationForm extends Component {

  constructor (props) {
    super(props)
    this.state = {
      form: {
        ...this.props.formData
      },
      error: {}
    }
    this.validateField = this.validateField.bind(this)
    this.shouldFieldBeVisible = this.shouldFieldBeVisible.bind(this)
  }

  componentWillReceiveProps(nextProps) {
    if (!_.isEqual(nextProps.formData, this.props.formData) && !_.isEqual(nextProps.formData, this.state.form)) {
      const form = {
        ...this.state.form,
        ...nextProps.formData
      }
      this.setState({
        form: form
      });
    }
  }

  onFieldValueChange (fieldName, type, validator, value) {
    const form = {
      ...this.state.form
    }
    if (type === 'dropdown') {
      form[fieldName] = value.value
    } else if (type === 'date') {
      form[fieldName] = moment(value).format('DD/MM/YYYY')
    } else {
      form[fieldName] = value
    }
    this.setState({
      form: form
    })

    this.props.updateFormData(form)

    setTimeout(() => {
      const dependentFields = _.uniq(validator.map((item) => {return item.field}))
      let shouldValidate = false;
      dependentFields.map((item) => {
        if (!shouldValidate) {
          if (this.state.error[item]) {
            shouldValidate = true
          }  
        }
        return item
      })
      if (shouldValidate) {
        this.setError.bind(this)(validator)
      }
    }, 100)
  }

  setError (validator) {
    const errorMessageList = this.validateField(validator)
    const errorObj = {
      ...this.state.error
    }
    _.map(errorMessageList, (error) => {
      if (error.errorMessage !== this.state.error[error.fieldName]) {
        errorObj[error.fieldName] = error.errorMessage
      }
    })

    this.setState({
      error: errorObj
    })
  }

  validateField (validator) {
    let error = []
    const dependentFields = _.uniq(validator.map((item) => {return item.field}))
    _.map(dependentFields, (item) => {
      error.push({
        errorMessage: '',
        fieldName: item
      })
    })

    _.map(validator, (item) => {
      const selectedErrorObjIndex = _.findIndex(error, (obj) => {
        return obj.fieldName === item.field
      })

      const hasError = (error[selectedErrorObjIndex].errorMessage) !== ''

      if (!hasError) {
        const result = this.props.validator[item.validationType](this.state.form[item.field], this.state.form)
        if (!result) {
          error[selectedErrorObjIndex].errorMessage = item.errorMessage
        }
      }
    })
    return error
  }
  
  next () {
    const errorObj = {
      ...this.state.error
    }
    const formObj = {
      ...this.state.form
    }
    let hasError = false
    _.forEach(this.props.formConfig, (value, key) => {
      if (!value.isCustom) {
        const isVisible = this.shouldFieldBeVisible(value.isVisible)
        if (isVisible) {
          const errorMessageList = this.validateField(value.validations)          

          _.map(errorMessageList, (error) => {
            if (error.errorMessage !== this.state.error[error.fieldName]) {
              errorObj[error.fieldName] = error.errorMessage
            }
            if (error.errorMessage) {
              hasError = true
            }
          })
      
        } else {
          delete errorObj[value.name]
          delete formObj[value.name]
        }
      }
    })
    this.setState({
      error: errorObj,
      form: formObj
    })
    this.props.updateFormData(formObj)
    this.props.next(hasError, formObj)
  }
  
  shouldFieldBeVisible (dependency) {
    let isVisible = true
    _.forEach(dependency, (value, key) => {
      if (isVisible) {
        const fieldVal = this.state.form[key];
        if (!value.includes(fieldVal)) {
          isVisible = false
        }  
      }
    })
    return isVisible
  }

  render () {
    const fieldsMap = _.map(this.props.formConfig, (field, index) => {
      const fieldNumber = `${index + 1}`
      let isVisible = this.shouldFieldBeVisible(field.isVisible || {})
      if (isVisible) {
        if (field.isCustom) {
          let childToRender
          if (_.isArray(this.props.children)) {
            childToRender = _.find(this.props.children, (data) => {
              return data.props.number === fieldNumber
            })
          } else {
            if (this.props.children.props.number === fieldNumber) {
              childToRender = this.props.children
            }
          }
          return (childToRender)
        }
        switch (field.type) {
          case 'dropdown':
            return (<Dropdown
              error={this.state.error[field.name]}
              value={this.state.form[field.name]}
              onFieldValueChange={this.onFieldValueChange.bind(this, field.name, field.type, field.validations)}
              onFieldBlur={this.setError.bind(this, field.validations)}
              options={field.options}
              label={field.label}
              name={field.name}
              key={`${field.name}_${fieldNumber}_dropdown`}
            />)
          case 'text':
          case 'number':
            return (<Textbox
              error={this.state.error[field.name]}
              type={field.type}
              value={this.state.form[field.name]}
              onFieldValueChange={this.onFieldValueChange.bind(this, field.name, field.type, field.validations)}
              onFieldBlur={this.setError.bind(this, field.validations)} 
              label={field.label}
              name={field.name}
              key={`${field.name}_${fieldNumber}_${field.type}`}
            />)
          case 'radioSingleLine':
          case 'radioMultiLine':
            return (<Radio
              error={this.state.error[field.name]}
              type={field.type}
              options={field.options}
              value={this.state.form[field.name]}
              onFieldValueChange={this.onFieldValueChange.bind(this, field.name, 'radio', field.validations)}
              label={field.label}
              name={field.name}
              key={`${field.name}_${fieldNumber}_radio`}
            />)
          case 'date': 
            return (<DateSelect
              error={this.state.error[field.name]}
              value={this.state.form[field.name] && this.state.form[field.name] !== 'Invalid date' ? moment(this.state.form[field.name], 'DD/MM/YYYY') : null}
              changeDate={this.onFieldValueChange.bind(this, field.name, field.type, field.validations)}
              onFieldBlur={this.setError.bind(this, field.validations)}  
              label={field.label}
              name={field.name}
              minimumGapInYears={field.minimumGapInYears}
              key={`${field.name}_${fieldNumber}_date`}
            />)
          default:
            return (<div key={`${field.name}_${fieldNumber}_div`} />)    
        }
      }
    })
    return (<div>
      {fieldsMap}
      <Row>
        <Col xs={12} id="application-form-next-wrapper">
          <Button
            id="application-form-next" 
            key="next" 
            bsStyle="primary" 
            onClick={this.next.bind(this)}>
            <FormattedMessage id={'common.next'} />
          </Button>
        </Col>
      </Row>
    </div>)
  }
}

ApplicationForm.propTypes = {
  formConfig: PropTypes.array.isRequired,
  validator: PropTypes.object.isRequired,
  formData: PropTypes.object,
  children: PropTypes.any,
  updateFormData: PropTypes.func.isRequired,
  next: PropTypes.func.isRequired
}
export default ApplicationForm
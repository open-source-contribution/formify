import Select from 'react-select'
import _ from 'lodash'
import { injectIntl, intlShape, FormattedMessage } from 'react-intl'
import { Row, Col, ControlLabel, FormGroup, HelpBlock } from 'react-bootstrap'
import React from 'react'
import PropTypes from 'prop-types'
import classes from './dropdown.css'

const Dropdown = ({
  intl,
  onFieldValueChange,
  value,
  options,
  label,
  locked,
  name,
  error,
  placeholder,
  onFieldBlur
}) => {
  const { formatMessage } = intl
  let dropDownOptions = options.map(opt => ({
    value: opt.value,
    extra: opt.extra,
    label: formatMessage({ id: opt.name, defaultMessage: opt.name })
  }))
  dropDownOptions = _.sortBy(dropDownOptions, ['label'])

  let validationState
  if (error) {
    validationState = 'error'
  }
  const selectedOption = _.find(dropDownOptions, (item) => {
    return item.value === value
  })
  return (<FormGroup validationState={validationState}>
    <Row key={`${name}-row11`}>
      {label && <Col
        key={`${name}-col11`}
        componentClass={ControlLabel} xs={12}
      >
        <FormattedMessage id={label} />
      </Col>}
      <Col
        key={`${name}-col12`}
        xs={12} className="padding-0"
      >
        <Row key={`${name}-row21`}>
          <Col xs={12} key={`${name}-col21`}>
            <Select
              name="form-field-name"
              value={selectedOption}
              options={dropDownOptions}
              onChange={onFieldValueChange}
              onBlur={onFieldBlur}
              searchable={false}
              clearable={false}
              refs="selectList"
              placeholder={formatMessage({ id: placeholder || 'dropdown.select', defaultMessage: 'Select' })}
              disabled={locked}
              key={`${name}-select`}
            />
            {locked && <div className={classes.locked} />}
          </Col>
        </Row>
      </Col>
      {error && <Col xs={12} key={`${name}-col13`}>
        <HelpBlock key={`${name}-helpBlock`}>
          <FormattedMessage id={error} key={`${name}-formatMessage`} />
        </HelpBlock>
      </Col>}
    </Row>
  </FormGroup>)
}

Dropdown.propTypes = {
  intl: intlShape.isRequired,
  onFieldValueChange: PropTypes.func.isRequired,
  value: PropTypes.string,
  options: PropTypes.array.isRequired,
  label: PropTypes.string,
  locked: PropTypes.bool,
  name: PropTypes.string,
  error: PropTypes.string,
  placeholder: PropTypes.string,
  onFieldBlur: PropTypes.func.isRequired
}

export default injectIntl(Dropdown)

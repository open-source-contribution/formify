import React from 'react';
import Dropdown from './dropdown'
import {shallowWithIntl} from '../../../helpers/intl-test'


describe('Dropdown', () => {
  it('renders without crashing', () => {
    const wrapper = shallowWithIntl(<Dropdown
      error={''}
      value={''}
      onFieldValueChange={() => {}}
      onFieldBlur={() => {}}
      options={[{
        "name": "form.NATIONALITY.option1",
        "value": "Indian"
      },
      {
        "name": "form.NATIONALITY.option2",
        "value": "American"
      }]}
      label={'form.NATIONALITY.label'}
      name={'NATIONALITY'} />
    );
    expect(wrapper).toBeDefined();
  })
})
import React from 'react';
import DateSelect, {disabledDate} from './date-select'
import {shallowWithIntl} from '../../../helpers/intl-test'
import moment from 'moment'


describe('DateSelect', () => {
  it('renders without crashing', () => {
    const wrapper1 = shallowWithIntl(<DateSelect 
      error={'form.error.required'}
      value={moment('02/02/2018', 'DD/MM/YYYY')}
      changeDate={() => {}}
      onFieldBlur={() => {}}  
      label={'form.BIRTH_DATE.label'}
      name={'BIRTH_DATE'}
      minimumGapInYears={20} />
    );
    expect(wrapper1).toBeDefined();

    const wrapper2 = shallowWithIntl(<DateSelect 
      error={'form.error.required'}
      value={null}
      changeDate={() => {}}
      onFieldBlur={() => {}}  
      label={'form.BIRTH_DATE.label'}
      name={'BIRTH_DATE'}
      minimumGapInYears={20} />
    );
    expect(wrapper2).toBeDefined();
  });

  it('disabled date function should return a function that return true whenever (today date - date chosen < minimum years gap required)', () => {
    const fn = disabledDate(20);

    // return false with no date selection
    expect(fn()).toEqual(false)
    expect(fn(moment('02/02/1991', 'DD/MM/YYYY'))).toEqual(false)
  })

})
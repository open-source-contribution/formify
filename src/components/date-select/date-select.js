import React from 'react'
import PropTypes from 'prop-types'
import { Row, Col, ControlLabel, HelpBlock, FormGroup } from 'react-bootstrap'
import { FormattedMessage, injectIntl } from 'react-intl'
import 'rc-calendar/assets/index.css'
import CalendarLocaleUS from 'rc-calendar/lib/locale/en_US'
import DatePicker from 'rc-calendar/lib/Picker'
import Calendar from 'rc-calendar'
import moment from 'moment';


export function disabledDate (minimumGapInYears) {
  return (current) => {
    if (!current) {
      // allow empty select
      return false;
    }
    const date = moment();
    date.hour(0);
    date.minute(0);
    date.second(0);
    return date.diff(current, 'years') < minimumGapInYears  // can not select days before today  
  }
}

const DateSelect = ({ changeDate, value, label, name, error, onFieldBlur, minimumGapInYears }) => {
  let validationState
  if (error) {
    validationState = 'error'
  }

  const calendar = (<Calendar
    disabledDate={minimumGapInYears ? disabledDate(minimumGapInYears) : () => {}}
    defaultValue={minimumGapInYears ? moment().subtract(minimumGapInYears, 'years') : moment()}
    locale={CalendarLocaleUS} style={{
      zIndex: 1000
    }}
  />)
  return (
    <FormGroup validationState={validationState}>
      <Row key={`${name}-row11`}>
        <Col
          key={`${name}-col11`}
          componentClass={ControlLabel} xs={12}
        >
          <FormattedMessage id={label} key={`${name}-formatMessage`} />
        </Col>
        <Col
          key={`${name}-col12`}
          xs={12} className="padding-0"
        >
          <Row key={`${name}-row21`}>
            <Col xs={12} key={`${name}-col21`}>
              <DatePicker 
                animation="slide-up" 
                calendar={calendar} 
                onChange={changeDate} 
                value={value} 
                onOpenChange={(isOpen) => {
                  if(!isOpen) {
                    onFieldBlur()
                  }
                }}>
                {({value}) => {
                  return (<span tabIndex="0">
                    <input
                      onChange={()=>{}}
                      placeholder="" 
                      tabIndex="-1" 
                      className="form-control" 
                      value={value ? moment(value).format('DD/MM/YYYY') : ''}
                    />
                  </span>)
                  }}
              </DatePicker>
            </Col>
            {error && <Col xs={12}>
              <HelpBlock key={`${name}-helpBlock`}>
                <FormattedMessage id={error} key={`${name}-formatMessage`} />
              </HelpBlock>
            </Col>}
          </Row>
        </Col>
      </Row>
    </FormGroup>
  )
}

DateSelect.propTypes = {
  changeDate: PropTypes.func.isRequired,
  onFieldBlur: PropTypes.func.isRequired,
  value: PropTypes.object,
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  error: PropTypes.string,
  minimumGapInYears: PropTypes.number
}

export default injectIntl(DateSelect)

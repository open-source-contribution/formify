import React from 'react';
import Textbox from './textbox'
import {shallowWithIntl} from '../../../helpers/intl-test'


describe('Textbox', () => {
  it('renders without crashing', () => {
    const wrapper = shallowWithIntl(<Textbox
      error={''}
      type={'text'}
      value={''}
      onFieldValueChange={() => {}}
      onFieldBlur={() => {}} 
      label={'form.FIRST_NAME.label'}
      name={'FIRST_NAME'} />);
    expect(wrapper).toBeDefined();
  })
})
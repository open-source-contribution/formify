import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage, injectIntl, intlShape } from 'react-intl'
import {
  ControlLabel,
  Col,
  HelpBlock,
  FormControl,
  Row,
  FormGroup
} from 'react-bootstrap'


class Textbox extends Component {

  render() {
    const { formatMessage } = this.props.intl
    const {error, type, label, value, name, disabled, placeholder, onFieldValueChange, onFieldBlur} = this.props
    let validationState
    if (error) {
      validationState = 'error'
    }
    return (
      <FormGroup controlId={name} validationState={validationState}>
        <Row key={`${name}-formGroup`}>
          {this.props.label && <Col
            key={`${name}-col11`}
            componentClass={ControlLabel} xs={12}
          >
            <FormattedMessage id={label} />
          </Col>}
          <Col
            key={`${name}-col12`}
            xs={12} className="padding-0"
          >
            <Row key={`${name}-row21`}>
              <Col xs={12} key={`${name}-col21`}>
                <FormControl
                  type={type}
                  value={value ? value : ''}
                  onChange={(event) => {
                    onFieldValueChange(event.target.value)
                  }}
                  key={`${name}-formControl`}
                  disabled={disabled}
                  onBlur={onFieldBlur}
                  placeholder={placeholder ? formatMessage({ id: placeholder }) : ''}
                />
              </Col>
            </Row>
          </Col>
          {error && <Col xs={12}>
            <HelpBlock key={`${name}-helpBlock`}>
              <FormattedMessage id={error} key={`${name}-formatMessage`} />
            </HelpBlock>
          </Col>}
        </Row>
      </FormGroup>
    )
  }
}

Textbox.propTypes =  {
  type: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  error: PropTypes.string,
  value: PropTypes.string,
  name: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
  placeholder: PropTypes.string,
  intl: intlShape.isRequired,
  onFieldValueChange: PropTypes.func.isRequired,
  onFieldBlur: PropTypes.func.isRequired
}

export default injectIntl(Textbox)
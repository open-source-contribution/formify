/* eslint-disable no-nested-ternary*/
import moment from 'moment'


// return false if not valid value
function required(val) {
  if (!val && val !== '0' && val !== 0) {
    return false
  }
  return true
}

function ageLimit(date) {
  const years = moment().diff(moment(date, 'DD/MM/YYYY'), 'year')
  if (years < 20) {
    return false
  }
  if (years > 65) {
    return false
  }
  return true
}

function checkValidDate(value) {
  if (!value) {
    return true
  }
  if (!moment(value, 'DD/MM/YYYY').isValid()) {
    return false
  }
  if (!(value.indexOf('/') > 0 &&
  value.substring(value.indexOf('/') + 1, value.lastIndexOf('/')).length > 0 &&
  value.substring(value.lastIndexOf('/') + 1, value.length).length > 0)) {
    return false
  }
  return true
}

function checkValidYear(value) {
  if (!value) {
    return true
  }
  // it should be four digits
  if (!(value.substring(value.lastIndexOf('/') + 1, value.length).length === 4)) {
    return false
  }
  return true
}

function stayLessThanAge(value, formData) {
  if (value && !formData['BIRTH_DATE']) {
    return false
  }
  const age = moment().diff(moment(formData['BIRTH_DATE'], 'DD/MM/YYYY'), 'years')
  if (value && value > age) {
    return false
  }
  return true
}

export default {
  required,
  ageLimit,
  checkValidDate,
  checkValidYear,
  stayLessThanAge
}

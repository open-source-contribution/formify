export default {
  "step1": [{
    "name": "TITLE_CODE",
    "label": "form.TITLE_CODE.label",
    "type": "radioSingleLine",
    "options": [{
        "name": "form.TITLE_CODE.option1",
        "value": "MR."
      },
      {
        "name": "form.TITLE_CODE.option2",
        "value": "MRS."
      },
      {
        "name": "form.TITLE_CODE.option3",
        "value": "MISS."
      }
    ],
    "validations": [{
      "validationType": "required",
      "errorMessage": "form.error.required",
      "field": "TITLE_CODE"
    }]
  }, {
    "name": "FIRST_NAME",
    "label": "form.FIRST_NAME.label",
    "type": "text",
    "validations": [{
      "validationType": "required",
      "errorMessage": "form.error.required",
      "field": "FIRST_NAME"
    }]
  }, {
    "isCustom": true,
    "name": "MOBILE_NUMBER",
    "label": "form.MOBILE_NUMBER.label",
    "type": "number",
    "validations": [{
      "validationType": "required",
      "errorMessage": "form.error.required",
      "field": "MOBILE_NUMBER"
    }]
  }, {
    "name": "NATIONALITY",
    "label": "form.NATIONALITY.label",
    "type": "dropdown",
    "defaultValue": "001",
    "options": [{
        "name": "form.NATIONALITY.option1",
        "value": "Indian"
      },
      {
        "name": "form.NATIONALITY.option2",
        "value": "American"
      }
    ],
    "context": "noChange",
    "validations": [{
      "validationType": "required",
      "errorMessage": "form.error.required",
      "field": "NATIONALITY"
    }]
  }, {
    "name": "BIRTH_DATE",
    "label": "form.BIRTH_DATE.label",
    "type": "date",
    "minimumGapInYears": 20,
    "validations": [{
        "validationType": "required",
        "errorMessage": "form.error.invalidDate",
        "field": "BIRTH_DATE"
      },
      {
        "validationType": "checkValidDate",
        "errorMessage": "form.error.invalidDate",
        "field": "BIRTH_DATE"
      },
      {
        "validationType": "checkValidYear",
        "errorMessage": "form.error.yearLimit",
        "field": "BIRTH_DATE"
      },
      {
        "validationType": "ageLimit",
        "errorMessage": "form.error.ageLimit",
        "field": "BIRTH_DATE"
      },
      {
        "validationType": "stayLessThanAge",
        "errorMessage": "form.error.stayLessThanAge",
        "field": "ADDRESS_STAY_DURATION"
      }
    ]
  }, {
    "isCustom": true,
    "name": "ADDRESS",
    "label": "form.ADDRESS.label",
    "type": "text",
    "validations": [{
      "validationType": "required",
      "errorMessage": "form.error.required",
      "field": "ADDRESS"
    }]
  }, {
    "name": "ADDRESS_STAY_DURATION",
    "label": "form.ADDRESS_STAY_DURATION.label",
    "type": "number",
    "validations": [{
      "validationType": "required",
      "errorMessage": "form.error.required",
      "field": "ADDRESS_STAY_DURATION"
    }, {
      "validationType": "stayLessThanAge",
      "errorMessage": "form.error.stayLessThanAge",
      "field": "ADDRESS_STAY_DURATION"
    }]
  }, {
    "name": "MARTITAL_STATUS",
    "label": "form.MARTITAL_STATUS.label",
    "type": "radioSingleLine",
    "options": [{
        "name": "form.MARTITAL_STATUS.option1",
        "value": "single"
      },
      {
        "name": "form.MARTITAL_STATUS.option2",
        "value": "married"
      }
    ],
    "validations": [{
      "validationType": "required",
      "errorMessage": "form.error.required",
      "field": "MARTITAL_STATUS"
    }]
  }, {
    "name": "SPOUSE_NAME",
    "label": "form.SPOUSE_NAME.label",
    "type": "text",
    "isVisible": {
      "MARTITAL_STATUS": ["married"]
    },
    "validations": [{
      "validationType": "required",
      "errorMessage": "form.error.required",
      "field": "SPOUSE_NAME"
    }]
  }]
}
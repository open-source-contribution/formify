This tool is meant to speed up the process of form creation by providing out-of-box form component with validations, custom css and custom fields support.

This tool expects certain inputs and does form rendering for you. It takes care of validation (onBlur, onSubmit) and display form fields error in expected language.  

Let's discuss features in detail
## List of features supported
 **Out of box form components**
 
 * formify-tool provides support for following data-types: dropdown, date, string, number and radio.
 * It handles form fields rendering based on input config. 
 
 **Validations support**
 
 * formify-tool handles field validation. Validation functions are provided as input to component.
 * It auto trigger field validation on field blur or on form submit.
 
 **Custom css**
 
 * formify tool uses react-bootstrap behind the scenes. However you can override react-bootstrap css as per react-bootstrap guidelienes

 **Custom fields**
 
 * formify tool provides support for adding your own custom field like digital signature.

## How to use formify-tool
```
<ApplicationForm 
    formConfig={formConfig.step1} 
    validator={validator}
    formData={formData}
    updateFormData={updateFormData}
    next={next} 
/>
```
**formConfig prop**

formConfig provides detailed information about form.  
1. Sequence of fields to be displayed
2. Fields name, data types, label, 
3. Fields validations if any
4. Options for selection (in case of dropdown and radio)
5. isCustom flag (turned on for custom fields)
6. Conditional field support through isVisible key (eg: SPOUSE_NAME field to be visible when MARITAL_STATUS is married)

Please find syntax for [formConfig](https://gitlab.com/open-source-contribution/formify/blob/master/src/assets/fixtures/fields.js)

**validator prop**

It contains list of functions that are triggered by formify-tool based on name mapping in formConfig.

Each validation function is called with two arguments: current field value and overall form value (needed for coupled field validation)

Please find syntax for [validator](https://gitlab.com/open-source-contribution/formify/blob/master/src/assets/fixtures/validator.js)

**formData prop**

formData prop is useful for providing default values to form. It is also used to provide custom field values to automated form.

It is an object with keys as field names

**updateFormData prop**

updateFormData prop is reponsible for syncing automated form data with custom form data. It is mainly needed when custom field validations required automated form fields values and vice-versa.

updateFormData looks like : 
``` 
 updateFormData (formData) {
    this.setState({
      formData: {
        ...formData
      }
    })
  }
```

**next prop**

formify-tool automatically calls next function with two args (hasError, formData) whenever submit button is clicked. 

hasError tells whether validations ran successfully for automated form
formData is the data that has been collected so far from form

## Custom fields support
Custom fields like digital signature can be added in your form generated using formify tool. 
formify-tool doesn't take care of validations, internationalization support for custom fields.

**formConfig for custom field**
```
{
    step1: [...,
      {
        // 4th index element (index starts from 0)
        "isCustom": true,
        "name": "ADDRESS",
        "label": "form.ADDRESS.label",
        "type": "text",
        "validations": [{
            "validationType": "required",
            "errorMessage": "form.error.required",
            "field": "ADDRESS"
        }]
      },
    ...
    ]
}
```

**html tags for custom field support**

````
<ApplicationForm 
    formConfig={formConfig.step1} 
    validator={validator} 
    formData={formData} 
    updateFormData={updateFormData}
    next={next}>
    <div number="5">
        // Your html code for field displayed at 5th position
    </div>
</ApplicationForm>    
````

